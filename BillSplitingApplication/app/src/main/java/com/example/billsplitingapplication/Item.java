package com.example.billsplitingapplication;

import java.util.ArrayList;

public class Item {

    String name;
    int priceInCents;
    int taxInCents;

    static ArrayList<Item> items = new ArrayList<>();

    public Item(String name, int price, int tax) {
        this.name = name;
        this.priceInCents = price;
        this.taxInCents = tax;
    }

}

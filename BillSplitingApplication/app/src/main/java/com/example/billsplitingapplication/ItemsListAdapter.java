package com.example.billsplitingapplication;

import android.annotation.SuppressLint;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.ViewHolder> {

    public OnClickItemsList onClickItemsList = null;

    @NonNull
    @Override
        public ViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
            // Creates the layout inflater object
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            if ((position < 0) || (position >= items.size())) {
                System.out.println("Error: position " + position + " in OrdersListAdapter is invalid.");
                return;
            }

                Item item = items.get(position);
                String positionString = String.valueOf(position + 1);
                holder.itemNumberTextView.setText(positionString);
                holder.itemNameTextView.setText(item.name);
                @SuppressLint("DefaultLocale") String dollarString = String.format("$%,.2f", (double) (item.priceInCents + item.taxInCents) / 100.0);
                holder.itemPriceTextView.setText(dollarString);
                holder.moreImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = holder.getAdapterPosition();
                        if ((onClickItemsList != null) && (position >= 0) && (position < items.size())) {
                            onClickItemsList.onClickOrder(position, items.get(position));
                        }
                    }
                });



        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        interface OnClickItemsList {
            void onClickOrder(int position, Item item);
        }

         ItemsListAdapter(ArrayList<Item> items) {
            this.items = items;
        }

        private ArrayList<Item> items;

        public static class ViewHolder extends RecyclerView.ViewHolder {

            TextView itemNumberTextView;
            TextView itemNameTextView;
            TextView itemPriceTextView;
            ImageButton moreImageButton;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                // Connects XML
                itemNumberTextView = itemView.findViewById(R.id.textView_item_number);
                itemNameTextView = itemView.findViewById(R.id.textView_item_name);
                itemPriceTextView = itemView.findViewById(R.id.textView_item_price);
                moreImageButton = itemView.findViewById(R.id.imageButton_more);
            }

        }

}


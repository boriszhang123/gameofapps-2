package com.example.billsplitingapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up recycler view
        RecyclerView ordersList = findViewById(R.id.recyclerView_orders);
        ItemsListAdapter itemsListAdapter = new ItemsListAdapter(Item.items);
        ordersList.setAdapter(itemsListAdapter);
        ordersList.setLayoutManager(new LinearLayoutManager(this));
        itemsListAdapter.onClickItemsList = new ItemsListAdapter.OnClickItemsList() {
            @Override
            public void onClickOrder(int position, Item item) {
                if ((position < 0) || (position >= Item.items.size())) { return; }
                showEditDeleteDialog(position);
            }
        };

        setupSplitButton();
    }

    private void showEditDeleteDialog(int position) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (ADD_EDIT_ITEM) : {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String name = data.getStringExtra("name");
                    int priceInCents = data.getIntExtra("priceInCents", 0);
                    int taxInCents = data.getIntExtra("taxInCents", 0);
                    int position = data.getIntExtra("position", -1);

                    int startingRow = 0;
                    if (position == -1) {
                        // Added a new item
                        Item item = new Item(name, priceInCents, taxInCents);
                        Item.items.add(0, item);
                        itemsListAdapter.notifyItemInserted(0);
                        startingRow = 1;  // No need to refresh newly added row
                    }
                    else {
                        Item item = Item.items.get(position);
                        item.name = name;
                        item.priceInCents = priceInCents;
                        item.taxInCents = taxInCents;
                    }

                    // Refresh remaining items so they show correct row numbers and/or updated values
                    for (int i=startingRow; i<Item.items.size(); i++) {
                        itemsListAdapter.notifyItemChanged(i);
                    }

                    // Update total price and item count
                    displayTotals();
                }
                break;
            }
        }
    }

    private final int ADD_EDIT_ITEM = 100;
    private ItemsListAdapter itemsListAdapter = null;
    private int billTotalInCents = 0;



    private void navigateToSplitBillScreen() {
        Intent intent = new Intent(this, split_bill_screen.class);
        intent.putExtra("billTotalInCents", billTotalInCents);
        startActivity(intent);
    }




    private void displayTotals() {
        TextView itemCountTextView = findViewById(R.id.textView_number_items);
        String itemCountString = String.valueOf(Item.items.size());
        if (Item.items.size() == 1)  {
            itemCountString += " item";
        }
        else {
            itemCountString += " items";
        }
        itemCountTextView.setText(itemCountString);

        billTotalInCents = 0;
        for (int i = 0; i < Item.items.size(); i++) {
            billTotalInCents += Item.items.get(i).priceInCents;
            billTotalInCents += Item.items.get(i).taxInCents;
        }
        double billTotal = Double.valueOf(billTotalInCents) / 100.0;
        String billTotalString = String.format("$%,.2f", billTotal);
        TextView totalTextView = findViewById(R.id.textView_dollar_amount);
        totalTextView.setText(billTotalString);
    }

    private void setupSplitButton() {
        Button splitButton = findViewById(R.id.button_split_bill);
        splitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToSplitBillScreen();
            }
        });
    }


}
